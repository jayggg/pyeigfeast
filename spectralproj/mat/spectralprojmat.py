"""
Class "Vecs" represent span objects made of numpy arrays.
Class "SpectralProjMat" represent spectral projectors of matrix pencils
represented as numpy arrays.
"""

from ..spectralproj import Span, SpectralProj
import numpy as np
from scipy.sparse.linalg import factorized
from scipy.sparse.linalg import splu


class Vecs(Span):

    """Class of spans of n vectors in R^m

            y = [ y_1, y_2, ... y_n ]     with y_i in R^m.
    """

    def __init__(self, n, m):  # constructor sets y = [0 ... 0]
        super().__init__(n, m)
        self.data = np.zeros((n, m), dtype=complex)

    def remove(self, cut):
        keep = list(set(range(self.m)).difference(cut))
        self.data = self.data[:, keep]
        self.m = self.m - len(cut)

    def scale(self, i, a):
        self.data[:, i] *= a

    def orthoreduce(self, tol=1.e-9):

        # Use QR factorization to find orthogonal basis for span
        q, r = np.linalg.qr(self.data)
        self.data = q

        # Remove those vectors which do not add substantially to span
        cut = []
        for k in range(r.shape[0]):
            if abs(r[k, k]) < tol:
                print('   Vec %d ~ linearly dependent.' % k)
                cut.append(k)
        if len(cut):
            print('   Removing %d vector(s).' % len(cut))
            self.remove(cut)

        return self, r

    def augment(self):

        mnew = min(2*self.m, self.n)
        d = mnew - self.m
        n = self.n
        self.data[:, self.m:] = \
            np.random.rand(n, d) + 1j * np.random.rand(n, d)
        self.m = mnew
        return self

    def __imul__(self, other):  # y *= u  where u is n x k or scalar

        if isinstance(other, (int, float, complex)):
            self.data *= other
        elif isinstance(other, np.ndarray):
            self.data = self.data.dot(other)
            self.m = other.shape[1]
        else:
            raise NotImplementedError()
        return self

    def __iadd__(self, other):  # y += x

        if isinstance(other, np.ndarray):
            self.data += other
        elif isinstance(other, Vecs):
            self.data += other.data
        else:
            raise NotImplementedError()
        return self

    def zeroclone(self):       # create new memory and zero
        return Vecs(self.n, self.m)

    def setrandom(self):       # set random complex values
        self.data = np.random.rand(self.n, self.m) + \
            1j * np.random.rand(self.n, self.m)
        return self

    def set(self, array):      # set y to  given array
        self.data = array
        self.n, self.m = array.shape
        return self

    def draw(self):
        # dont know how to draw a numpy vector, so do nothing
        return self


class SpectralProjMat(SpectralProj):

    """Class of spectral projections for m x m matrix pencil (A,B) in R^m.

    P = (2 pi i)^-1  integral_over_contour  (B*z-A)^-1 * B  dz

    """

    def __init__(self, A, B, **kwargs):
        """Construct the spectral projection for an m x m input matrix 'A'
        based on a circular contour of given 'center' and 'radius'.
        The H-inner product is given by another input m x m positive
        definite matrix 'B'. We discretize the contour integral using
        the trapezoidal rule with 'npts' number of points. The remaining
        kwargs are as documented in base class.
        """
        self.A = A
        self.B = B

        super().__init__(**kwargs)
        self.make_resolvents()

    def rayleigh(self, q):
        qAq = np.transpose(np.conjugate(q.data)).dot(self.A.dot(q.data))
        qBq = np.transpose(np.conjugate(q.data)).dot(self.B.dot(q.data))
        return (qAq, qBq)

    def rayleigh_nsa(self, ql, qr, qAq=not None, qBq=not None):
        if qAq is not None:
            qAq = np.transpose(np.conjugate(ql.data)).dot(self.A.dot(qr.data))
        if qBq is not None:
            qBq = np.transpose(np.conjugate(ql.data)).dot(self.B.dot(qr.data))
        return (qAq, qBq)

    def make_resolvents(self):

        # Compute factorizations of z B - A using scipy
        self.R = []
        for z in self.z:
            self._prnt('SpectralProjMat:   ' +
                       'Factorizing at z = %+7.3f% + 7.3fj'
                       % (z.real, z.imag))
            self.R.append(factorized(z * self.B - self.A))

    def check_resolvent_factorizations(self):
        """A debugging tool:
        Recompute LU factorizations that made the resolvent to check
        the U-factor for nearness to singularities.

        """

        print('SpectralProjMat: Checking LU factors of shifted system')
        for z in self.z:
            self._prnt('SpectralProjMat:   ' +
                       'Factorizing at z = %+7.3f% + 7.3fj'
                       % (z.real, z.imag))
            inv = splu(z * self.B - self.A)
            minu = min([abs(inv.U[i, i]) for i in range(inv.U.shape[0])])
            self._prnt('SpectralProjMat:     ' +
                       'Smallest diag(abs(U)) in U-factor = %g' % minu)

    def matrix_filter(self):
        """
        Return the matrix approximation S_N of the exact spectral
        projector S, as a dense numpy matrix. (Memory intensive!)
        """

        m = self.A.shape[0]
        y = Vecs(m, m)
        y.set(np.identity(m, dtype=complex))
        Py = self*y
        return Py

    def __mul__(self, v):   # P * v
        Pv = v.zeroclone()
        for z, w, resolvent in zip(self.z, self.w, self.R):
            summand = w * resolvent(self.B.dot(v.data))
            Pv += summand.reshape(v.data.shape)
        return Pv

    def __rmul__(self, v):  # v * P
        Pv = v.zeroclone()
        for z, w, resolvent in zip(self.z, self.w, self.R):
            summand = np.conjugate(w) \
                * resolvent(np.conjugate(self.B.T).dot(v.data), trans='H')
            Pv += summand.reshape(v.data.shape)
        return Pv

    def residual(self, lam, y, yl=None):

        if yl is not None:
            return self.residual_nsa(y, yl)

        r = self.A.dot(y.data) - self.B.dot(y.data).dot(np.diag(lam))
        res = np.zeros(y.m)
        for i in range(y.m):
            res[i] = np.linalg.norm(r[:, i])
        return res

    def residual_nsa(self, lam, lefty, righty):
        rightr = self.A.dot(righty.data) \
            - self.B.dot(righty.data).dot(np.diag(lam))
        leftr = np.conjugate(self.A.T).dot(lefty.data) \
            - np.conjugate(self.B.T).dot(lefty.data).dot(
            np.diag(np.conjugate(lam))
        )

        res = np.zeros(lefty.m)
        alph = np.abs(self.c) + self.r

        for i in range(len(res)):
            leftyi = lefty.data[:, i]
            rightyi = righty.data[:, i]

            leftnorm = np.linalg.norm(
                np.conjugate(self.B.T).dot(leftyi), ord=1)
            rightnorm = np.linalg.norm(self.B.dot(rightyi), ord=1)

            res[i] = max(
                np.linalg.norm(leftr[:, i], ord=1) / (alph * leftnorm),
                np.linalg.norm(rightr[:, i], ord=1) / (alph * rightnorm)
            )

        return res
