"""
Test SpectralProjNGR
"""

from netgen.geom2d import unit_square
from ngsolve import grad, dx
import ngsolve as ng
from pyeigfeast import SpectralProjNG, NGvecs
from pyeigfeast.spectralproj.ngs import SpectralProjNGR, ResolventAB
import numpy as np

ng.ngsglobals.msg_level = 1
inversetype = 'umfpack'

ngm0 = unit_square.GenerateMesh(maxh=0.49)
mesh0 = ng.Mesh(ngm0)
for i, bdries in enumerate(mesh0.GetBoundaries()):
    ngm0.SetBCName(i, 'allbdry')

X = ng.H1(mesh0, order=2, dirichlet='allbdry', complex=True)
u, v = X.TnT()


def test_spectralprojngr():

    a = ng.BilinearForm(X)
    a += (1+1j) * grad(u) * grad(v) * dx
    a.Assemble()

    b = ng.BilinearForm(X)
    b += u * v * dx
    b.Assemble()

    m = 1
    ctr = 19.0   # contour center
    rad = 40     # contour radius
    npts = 4     # number of points in trapezoidal rule

    P = SpectralProjNG(X, a.mat, b.mat, radius=rad, center=ctr, npts=npts,
                       reduce_sym=True, inverse=inversetype)
    Y = NGvecs(X, m)
    Y.setrandom(seed=1)

    W = NGvecs(X, m)
    W.setrandom(seed=1)

    P2 = SpectralProjNGR(lambda z: ResolventAB(X, a.mat, b.mat, z),
                         radius=rad, center=ctr, npts=npts,
                         reduce_sym=True, inverse=inversetype)

    PY = P * Y

    Y2 = Y.zeroclone()
    Y2 += Y
    PY2 = P2 * Y2

    D = PY.zeroclone()
    D._mv[:] = PY._mv - PY2._mv[:]

    d = ng.InnerProduct(D._mv, D._mv)
    print('Difference (should be 0)  =\n', d)
    assert np.linalg.norm(d) < 1e-15, \
        'SpectralProjNGR does not match equal SpectralProjNG'


test_spectralprojngr()
# WP = W * P   # adjoint action
# W2 = W.zeroclone()
# W2 += W

# W2P = W2 * P2
# W2P._mv[:] -= WP._mv
# print(ng.InnerProduct(W2P._mv, W2P._mv))

# ma2, mb2 = P2.rayleigh(W2)
# ma, mb = P.rayleigh(W)

# print(np.linalg.norm(ma2 - ma),
#       np.linalg.norm(mb2 - mb))
