"""
Tests on non-self-adjoint feast
"""

from pyeigfeast import SpectralProjMat, Vecs
import numpy as np
from scipy.sparse import csc_matrix


def nonherm_triu(N=5):
    A = np.triu(np.ones(N, dtype=np.complex128))
    B = np.eye(N, dtype=np.complex128)

    for i in range(N):
        A[i, i] = np.complex128(i + 1)

    A = csc_matrix(A)
    B = csc_matrix(A)
    return A, B


def nonherm_diag():
    A = np.diag([-1.0, 1.0, -1j, 1j])
    B = np.eye(A.shape[0])
    A = csc_matrix(A)
    B = csc_matrix(A)

    return A, B


def non_herm_feast(A, B, m, radius=3.0, center=3.0, npts=8, stop_tol=1.e-12,
                   niterations=1):
    # Create the spectral projector.
    P = SpectralProjMat(A=A, B=B, radius=radius, center=center, npts=npts)

    # Create spans for the left and right eigenspaces.
    lefty = Vecs(A.shape[0], m)
    lefty.setrandom()

    righty = Vecs(A.shape[0], m)
    righty.setrandom()

    lam, righty, history, lefty = P.feast(righty, Yl=lefty, stop_tol=stop_tol,
                                          niterations=niterations,
                                          hermitian=False)

    return lam, righty, lefty, history


def make_test_nonherm_mat(matrix_type='triu'):
    # Parameters for feast.

    stop_tol = 1.e-12        # the stopping tolerance for feast
    npts = 8                 # the number of quadrature points
    niterations = 100        # the number of iterations for the feast step

    # Specify the matrix type. The matrix type can be one of the following:
    #     triu - upper triangular
    #     diag - diagonal nonhermitian

    # Compute the matrices.
    if matrix_type == 'triu':
        m = 3
        N = 5
        radius = 1.5
        center = 3.0 + 0.0 * 1j

        A, B = nonherm_triu(N=N)

    elif matrix_type == 'diag':
        m = 2
        N = 4
        radius = 1.0
        center = 1.0 + 1.0 * 1j

        A, B = nonherm_diag()

    # Create the spectral projector object.
    # lam, lefty, righty, plam, mnew = \
    lam, righty, lefty, history = \
        non_herm_feast(A=A, B=B, m=m, radius=radius, center=center, npts=npts,
                       stop_tol=stop_tol, niterations=niterations)

    # Sort the computed eigenvalues in the manner indicated above.
    lam = np.sort(lam)

    # Get the eigenvalue differences.
    print(' history=', history)
    ewdiff = np.array(history[2][-1])
    max_err = np.max(np.abs(ewdiff))

    success = max_err < stop_tol
    errmsg = '\"{0}\" eigenvalues are incorrect!'.format(matrix_type)
    assert success, errmsg


def test_nonherm_mat_diag():
    make_test_nonherm_mat('diag')


def test_nonherm_mat_triu():
    make_test_nonherm_mat('triu')


test_nonherm_mat_diag()
test_nonherm_mat_triu()
