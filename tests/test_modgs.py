from pyeigfeast.spectralproj.ngs import NGvecs
import ngsolve as ng
import numpy as np
from netgen.geom2d import unit_square


def test_modgs():

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=0.1))
    V = ng.H1(mesh, order=2, complex=True)
    tol = 1e-13
    mvecs = 5
    seed = 123
    u, v = V.TnT()
    B = ng.BilinearForm(u*v*ng.dx)
    B.Assemble()

    nv = NGvecs(V, mvecs)
    nv.setrandom(seed)
    mv = nv._mv
    mv[2].data = (1 + tol) * mv[3]   # make one vector linearly dependent

    # Make a copy
    cv = nv.copy()

    # Modified GS
    r, swaps = nv.modifiedGScolpivot(M=B.mat, tol=tol)
    R = ng.Matrix(r)
    Q = mv

    # Test 1: Check that QR = AP
    for ij in swaps:    # make AP in cv
        i, j = ij
        cv._mv.Replace([i, j], cv._mv[(j, i)])
    AP = cv._mv

    dv = ng.MultiVector(mv[0], mvecs)
    dv.data = Q * R - AP
    diff1 = np.linalg.norm(np.array([ng.Norm(v) for v in dv]))

    # Test 2:
    # Cols of Q corresponding to nonzero R[i,i]) should be orthonormal
    i = np.argmax(abs(np.diag(r)) < tol)
    q = Q[:i]
    diff2 = np.linalg.norm(np.array(ng.InnerProduct(q, B.mat * q))
                           - np.eye(i))

    assert diff1 < 1e-13 and diff2 < 1e-13, 'Modified Gram Schmidt failed'


test_modgs()
