

l = load('dpgmatrices');  % Read in the output from pyeigfeast

B  = l.Xmatrix;           % This whole DPG matrix has the form:
%%        Ui  Qi  Yi 
%
%   B = [ 0   0   C']
%       [ 0   0   D']
%       [ C   D   G ]

fprintf('1. Check if DPG system is Hermitian: \n   ||B - B*|| = %e\n\n', ...
        norm(B - B', 'fro'))
Ui = intersect(l.dimcounts(1):l.dimcounts(2)-1, l.Xfree);
Qi = intersect(l.dimcounts(2):l.dimcounts(3)-1, l.Xfree);
Yi = l.dimcounts(3):l.dimcounts(4)-1;


G = B(Yi,Yi);             % G(k,l) = Y-inner product of e(k) and e(l).
GI = inv(G);   
C = B(Yi,Ui);             % C(k,i) = ( grad u(i), grad e(k) )
D = B(Yi,Qi);             % D(k,j) = < q(j) . n, e(k) >
S = l.U2Ytransfer(Yi,Ui); % S(k,i) = ( e(k),  u(i) )

DAD = D'*GI*D;
EA = full( C'*GI*C - C'*GI*D* (DAD \ (D'*GI*C) ) );
EC = full( C'*GI*S - C'*GI*D* (DAD \ (D'*GI*S) ) );

fprintf('2. Computing all eigenvalues using DPG full matrices:\n\n');
E=eig(-EA \ EC);
e = sort(1./ E);
e(1:4)

fprintf('3. Computing eigenvalues using standard FEM matrices:\n\n');
A = l.A + l.A' - diag(diag(l.A));  % Extract A, M from its saved 
M = l.M + l.M' - diag(diag(l.M));  % lower triangular parts.
Ao= A(l.Ufree, l.Ufree);           
Mo= M(l.Ufree, l.Ufree);
lams = eigs(Ao, Mo, 4, 'SM')


