"""2D EXAMPLE:                  [Requires ngsolve install]

Apply the FEAST algorithm to the matrix of the 2D Dirichlet problem on
the unit square. The matrices are made using NGSolve's finite element
implementation. The  operator A is defined by

(A u, v) = (grad u, grad v)

where (.,.) is the L^2 inner product. The Gram matrix of this L^2
inner product is  also made using NGsolve.
"""

# For feast implementation:
from pyeigfeast.spectralproj.ngs import NGvecs, SpectralProjNG
from pyeigfeast.scripts import exact_eig_unitsqr
from netgen.geom2d import unit_square
import numpy as np
import ngsolve as ngs
from ngsolve import grad, dx

# For direct eigenvalue computation by scipy:
from scipy.sparse.linalg import eigs
from scipy.sparse import coo_matrix
from scipy.linalg import norm


# FEAST with FEM for Dirichlet problems on general meshes

def eig_fem_feast(mesh, p, m, ctr, rad, npts,
                  verbose=True, quadrule='circ_trapez_shift',
                  check_contour=3, stop_tol=1.e-9,
                  reduce_by_symmetry=True, rhoinv=0, Y=None, niterations=50,
                  inverse=None):
    """ Apply FEAST with FEM discretization.

    lam, Y, history, P, a, b, X = eig_fem_feast(h, p, m, ctr, rad, npts)

    PARAMETERS

    mesh = ngsolve mesh where Dirichlet boundaries are marked 'allbdry'
    p = polynomial degree of finite element space
    m = number of vectors spanning the initial eigenspace iterate
    ctr = center of circular countour
    rad = radius of circular countour
    npts = number of quadrature points
    Y = Initial eigenspace (if not given, will be set randomly)
    reduce_by_symmetry: If True, remove reflected resolvent factorizations.
    quadrule='circ_trapez_shift': Select another available quadrature rule
    verbose=True:  False turns off too much outputs
    check_contour=n: check if eigenvalues in contour after n iterations
    niterations=N: perform N feast iterations without restart
    stop_tol=eps: stop if eigenvalues do not change by more than eps
    exactsol=exact_eig: if exact eigenvalues and eigenunctions are known,
                        then provide a function that returns them modeled
                        after exact_eig(...).
    OUTPUTS:

    lam = approximate eigenvalues computed by FEAST.
    Y = approximate eigenspan computed by FEAST.
    history: stored convergence history
    P = spectral projector
    a, b: left and right hand side matrices
    X = finite element space
    """

    ngs.ngsglobals.msg_level = 1
    X = ngs.H1(mesh, order=p, dirichlet='allbdry', complex=True)
    u, v = X.TnT()
    a = ngs.BilinearForm(X)
    a += grad(u) * grad(v) * dx
    b = ngs.BilinearForm(X)
    b += u * v * dx
    a.Assemble()
    b.Assemble()

    P = SpectralProjNG(X, a.mat, b.mat, radius=rad, center=ctr, npts=npts,
                       reduce_sym=reduce_by_symmetry, verbose=verbose,
                       inverse=inverse)

    if Y is None:
        Y = NGvecs(X, m, b.mat)
        Y.setrandom()

    lam, Y, history, _ = P.feast(Y, check_contour=check_contour,
                                 stop_tol=stop_tol,
                                 niterations=niterations)

    Y.draw()

    return lam, Y, history, P, a, b, X


# Direct eigensolve by SCIPY for comparison


def direct_eig(a, b, X, k=3):
    """
    Direct computation of eigenvalues using scipy's sparse eigs and
    NGSolve's matrices converted to scipy.sparse.
    """

    free = np.array(X.FreeDofs())
    freedofs = np.where(free)[0]       # free dofs as a numpy array:
    i, j, values = a.mat.COO()         # convert to scipy in COO format
    # ignore the zero imaginary part of the marices:
    avalues = np.array([av.real for av in values])
    A = coo_matrix((avalues, (i, j)))  # stiffness matrix without bc
    i, j, values = b.mat.COO()
    bvalues = np.array([bv.real for bv in values])
    B = coo_matrix((bvalues, (i, j)))  # mass matrix without bc
    A = A.tocsc()[:, freedofs]
    A = A.tocsr()[freedofs, :]         # stiffness matrix with bc
    B = B.tocsc()[:, freedofs]
    B = B.tocsr()[freedofs, :]         # mass matrix with bc

    ew, ev = eigs(A, k=k, M=B, which='SR')

    print('Compare with eigenvalues computed by scipy:\n', ew)
    return ew


###################################################################

if __name__ == '__main__':

    # PARAMETERS:

    h = 0.1      # mesh size for finite element discretization
    p = 2        # degree of Lagrange finite elements to use
    m = 4        # number of vectors in initial eigenspace iterate
    ctr = 19.0   # contour center
    rad = 40     # contour radius
    npts = 8     # number of points in trapezoidal rule

    # MESH:
    ngmesh = unit_square.GenerateMesh(maxh=h)
    mesh = ngs.Mesh(ngmesh)
    for i in range(len(mesh.GetBoundaries())):
        # mark Dirichlet bc parts as 'allbdry'
        ngmesh.SetBCName(i, 'allbdry')

    # FEAST:
    lam, Y, history, P, A, B, X = eig_fem_feast(mesh, p, m, ctr, rad, npts)

    # ERRORS:
    ew = np.sort(lam.real)
    exew, _ = exact_eig_unitsqr(ctr, rad)
    exew = exew[0, :]
    print("\nExact undiscretized Dirichlet eigenvalues:\n", exew)
    print("FEAST approximation:\n ", ew)
    im = norm(lam.imag, 2)
    if im > 1.e-10:
        print("Computed eigenvalues are not real!")
    if len(ew) == len(exew):
        print("|eigenvalue discretization error| = %g\n" % norm(ew-exew, 2))
    else:
        print("Computed and exact eigenspace dimensions differ!")

    # # COMPARE:
    # ewscipy = direct_eig(a, b, X, k=3)
    # l = min(len(ew), len(ewscipy))
    # ewerr = norm(ew[:l]-ewscipy[:l], 2)
    # print("\n|Difference b/w FEAST & SCIPY eigenvalues| = %g\n" % ewerr)
