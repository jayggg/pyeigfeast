"""
Study of convergence of discretization errors for the first 3
Dirichlet eigenvalues on the unit square obtained by the DPG/Feast
algorithm.

"""

from dirichletDPG import eig_dpg_feast_dir
from utils import print_apparent_rates, print_rates
from netgen.geom2d import unit_square
import ngsolve as ngs
import numpy as np


# PARAMETERS:

if __name__ == '__main__':

    p = 2          # polynomial degree of DPG discretization
    m = 4          # number of vectors in initial eigenspace iterate
    ctr = 20.0     # contour center
    rad = 40       # contour radius
    ewn = 3        # number of exact eigenvalues within the contour
    N = 8          # number of points in trapezoidal rule
    h0 = 0.25      # coarsest mesh size to begin
    maxr = 3       # number of successive refinements to consider

    efAerr = []
    ewerr = []
    hs = []
    ews = []
    res = []
    ngs.ngsglobals.msg_level = 0
    ngm0 = unit_square.GenerateMesh(maxh=h0)
    mesh = ngs.Mesh(ngm0)
    for i, bdries in enumerate(mesh.GetBoundaries()):
        ngm0.SetBCName(i, 'allbdry')

    for ll in range(maxr):

        h = h0 * 2**(-ll)
        hs.append(h)

        print("Computing ... case of level=%d, p=%d" % (ll, p))
        lam, Y, history, P = eig_dpg_feast_dir(mesh, p, m, ctr, rad, N,
                                               verbose=False)
        ewerrors, eferrors, ewchanges, residuals, ewhistory, cgd = history

        efAerr.append(eferrors[-1])
        ewerr.append(ewerrors[-1])
        res.append(residuals[-1])
        ews.append(lam)

        if len(lam) != ewn:
            print('\n***FEAST did not get the right eigenspace dimension!')
            print('   Try again with better h, p, N, or thresholds.')
            exit(1)

        mesh.Refine()

    ews = np.array(ews)
    ewerr = np.array(ewerr)
    efAerr = np.array(efAerr)
    np.savez('outputs/tmpunitsqr.npz', ews=ews, ewerr=ewerr, efAerr=efAerr,
             hs=hs, res=res)

    print_apparent_rates(ews, hs)
    print_rates(ewerr, efAerr, hs)
