""" 1D EXAMPLE:

Apply the FEAST algorithm to the matrix of 1D Dirichlet problem
obtained by the finite element approximation on the unit interval
using n elements of size h=1/(n+1).

"""


from pyeigfeast.spectralproj.mat import Vecs, SpectralProjMat
from numpy import ones, where, arange, cos, pi, sort
from scipy.linalg import norm
from scipy.sparse import diags
import numpy as np


def make_tridiag(n):            # MAKE THE MATRICES

    a = diags(-ones(n-1), 1)    # tridiagonal (-1,2-1) matrix
    a += diags(2*ones(n), 0)
    a += diags(-ones(n-1), -1)

    b = diags(ones(n), 0)       # matrix of base inner product

    return (a, b)


def run_feast_1dtridiag(n, m, ctr, rad, numtrap,
                        quadrule='circ_trapez_shift',
                        rhoinv=0.0, verbose=True,
                        niterations=30, check_contour=3):

    # CONSTRUCT SPECTRAL PROJECTION:

    a, b = make_tridiag(n)
    P = SpectralProjMat(a.tocsc(), b.tocsc(),
                        radius=rad, center=ctr, npts=numtrap,
                        quadrule=quadrule, rhoinv=rhoinv,
                        verbose=verbose)

    # RUN FEAST ALGORITHM:

    Y = Vecs(n, m).setrandom()
    lam, Y, history, _ = P.feast(Y, niterations=niterations,
                                 check_contour=check_contour)

    # COMPARE WITH KNOWN EXACT SOLUTION:

    k = arange(1, n+1)                   # exact ews of this discrete problem:
    exactew = 2 * (1 - cos(pi*k/(n+1)))
    exew = exactew[where(exactew > ctr-rad)]
    exew = exew[where(exew < ctr+rad)]   # exact ew lying in the search region

    ew = sort(lam.real)                  # computed eigenvalues
    print("\n* Result summary:\n Exact Dirichlet eigenvalues = \n", exew)
    print(" FEAST approximation = \n", ew)
    im = norm(lam.imag, 2)
    if im > 1.e-9:
        print(" Computed eigenvalues are not real!  norm(imag)=%g" % im)
    if len(ew) == len(exew):
        ewerr = norm(ew-exew, 2)
        print(" |Eigenvalue error| = %g\n" % ewerr)
    else:
        print(" Computed and exact eigenspace dimensions differ!")
        ewerr = None

    return (lam, Y, history, exew, ewerr, P)

###################################################################


if __name__ == '__main__':

    example = 'eg4'               # CHOOSE THE DEMO EXAMPLE TO RUN

    if example == 'eg1':          # Typical run

        print('EXAMPLE 1: Displaying iterative history of ew convergence')
        m = 3                     # initial number of vectors
        numtrap = 8               # number of trapezoidal rule points
        n = 100                   # mesh size is h = 1 / (n+1)
        ctr = 0.45                # contour center
        rad = 0.05                # contour radius
        print('  Size of generalized eigensystem =', n)
        print('  Dimension of initial random span =', m)
        print('  Number of points used in Butterowrth filter =', numtrap)
        print('  Center of circular countour =', ctr)
        print('  Radius of circular countour =', rad)

        lam, Y, history, exew, ewerr, P \
            = run_feast_1dtridiag(n, m, ctr, rad, numtrap)
        ewe, efe, ewc, res, ews, cgd = history
        print('\n* Iterative history of eigenvalue errors:\n', ews[:] - exew)

    elif example == 'eg2':        # number of eigenvalues > initial m

        print('EXAMPLE 2: Restarts if initial dim is too low')
        m = 5                     # initial number of vectors
        numtrap = 8               # number of trapezoidal rule points
        n = 100                   # mesh size is h = 1 / (n+1)
        ctr = 1.00                # contour center
        rad = 0.20                # contour radius
        print('  Size of generalized eigensystem =', n)
        print('  Dimension of initial random span =', m)
        print('  Number of points used in Butterowrth filter =', numtrap)
        print('  Center of circular countour =', ctr)
        print('  Radius of circular countour =', rad)
        run_feast_1dtridiag(n, m, ctr, rad, numtrap)

    elif example == 'eg3':

        print('EXAMPLE 3: Elliptical filter')
        m = 3                     # initial number of vectors
        numtrap = 4              # number of trapezoidal rule points
        n = 100                   # mesh size is h = 1 / (n+1)
        ctr = 1.0/3.0             # interval center
        rad = 1.0/17.0            # interval = [ctr-rad, ctr+rad]
        rhoinverse = 1.0/3.0      # ellipse eccentricity

        print('* Parameters:')
        print('  Size of generalized eigensystem =', n)
        print('  Dimension of initial random span =', m)
        print('  Number of points used in elliptical snug rule =', numtrap)
        print('  Eccentricty of ellipse: rhoinv = ', rhoinverse)
        print('  Center of circular countour =', ctr)
        print('  Radius of circular countour =', rad)

        lam, Y, history, exew, ewerr, P \
            = run_feast_1dtridiag(n, m, ctr, rad, numtrap,
                                  quadrule='ellipse_trapez_snug',
                                  rhoinv=1.0/3.0,
                                  niterations=1000, check_contour=1000)
        ewe, efe, ewc, res, ews, cgd = history
        print('\n* Iterative history of eigenvalue errors:\n',
              ews[:] - exew)

        print('\n* Check if resolvents were made of nearly singular systems:')
        P.check_resolvent_factorizations()

        print('\n* Make dense filter matrix S and compute spectrum:')
        S = P.matrix_filter()
        Sew, Sev = np.linalg.eig(S.data)
        print(' First few (sorted by abs value) eigenvalues of S:\n',
              Sew[:2*m].real)
        exactew = 2 * (1 - cos(pi * arange(1, n+1)/(n+1)))
        rew = P.filter(exactew)[0]
        print(' Numerical difference b/w spectrum(S) and r_N(spectrum(A)) =',
              np.linalg.norm(sort(rew.real) - sort(Sew.real)))

    elif example == 'eg4':

        print('EXAMPLE 4: Set custom/handmade elliptical contour')

        m = 5                     # initial number of vectors
        numtrap = 4               # number of trapezoidal rule points
        n = 100                   # mesh size is h = 1 / (n+1)
        ctr = 1.0/3.0             # interval center
        rad = 1.0/17.0            # interval = [ctr-rad, ctr+rad]
        rhoinv = 1.0/3.0          # ellipse eccentricity
        rho = 1/rhoinv

        # prepare inputs

        def within_ellipse(z):    # function to be given as input
            """ Given a numpy array z, return an array of true or false
            depending on whether each z-value is in the ellipse or not."""
            z0 = z - ctr
            x = z0.real
            y = z0.imag
            ell = (x / (rho + rhoinv))**2 + (y/(rho - rhoinv))**2
            inside = ell - (rad / (rho+rhoinv))**2 < 0
            return inside

        # quadrature points
        z = np.array([0.38415566+0.00000000e+00j,
                      0.33333333+4.06578642e-02j,
                      0.282511 + 4.97915233e-18j,
                      0.33333333-4.06578642e-02j])

        # complex quadrature weights
        w = np.array([1.01644661e-02+0.00000000e+00j,
                      6.22394041e-19+1.27055826e-02j,
                      -1.01644661e-02+1.55598510e-18j,
                      -1.86718212e-18-1.27055826e-02j])

        # pack them together for input
        quad = {'z': z, 'w': w, 'within': within_ellipse,
                'quadrule': 'my_elliptical_contour'}

        a, b = make_tridiag(n)
        P = SpectralProjMat(a.tocsc(), b.tocsc(), contourquad=quad)

        Y = Vecs(n, m).setrandom()
        lam, Y, history, _ = P.feast(Y, niterations=1000,
                                     check_contour=1)

        # COMPARE WITH KNOWN EXACT SOLUTION:

        # exact ews of this discrete problem:
        k = arange(1, n+1)
        exactew = 2 * (1 - cos(pi*k/(n+1)))
        exew = exactew[where(within_ellipse(exactew))]
        ew = sort(lam.real)                  # computed eigenvalues
        print("\n* Result summary:\n Exact Dirichlet eigenvalues = \n", exew)
        print(" FEAST approximation = \n", ew)
