"""
Module implementing FEAST eigensolver with various discretizations
"""

__all__ = [
    'SpectralProj', 'SpectralProjMat', 'SpectralProjDPG', 'Span', 'Vecs',
    'NGvecs', 'simple_multiple_zoom', 'SpectralProjNG', 'SpectralProjNGR',
    'ResolventAB', 'SpectralProjNGPoly'
]

from .spectralproj import Span, SpectralProj, simple_multiple_zoom
from .spectralproj.mat import Vecs, SpectralProjMat
from .spectralproj.dpg import NGvecs, SpectralProjDPG
from .spectralproj.ngs import SpectralProjNG
from .spectralproj.ngs import SpectralProjNGR, ResolventAB, SpectralProjNGPoly
